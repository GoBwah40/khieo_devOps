DROP DATABASE IF EXISTS khizo;
CREATE DATABASE IF NOT EXISTS khizo;

USE khizo;

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configuration` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `name` varchar(255) NOT NULL,
                                 `value` int DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (1,'maxFriends',10);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
                        `id` bigint NOT NULL AUTO_INCREMENT,
                        `lastname` varchar(255) NOT NULL,
                        `firstname` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `ban_date` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `friendship`
--

DROP TABLE IF EXISTS `friendship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friendship` (
                              `id` bigint NOT NULL AUTO_INCREMENT,
                              `id_sender` bigint NOT NULL,
                              `id_receiver` bigint NOT NULL,
                              PRIMARY KEY (`id`),
                              KEY `friendship_users_null_fk` (`id_sender`),
                              KEY `friendship_users_null_fk2` (`id_receiver`),
                              CONSTRAINT `friendship_users_null_fk` FOREIGN KEY (`id_sender`) REFERENCES `users` (`id`),
                              CONSTRAINT `friendship_users_null_fk2` FOREIGN KEY (`id_receiver`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friendship`
--

LOCK TABLES `friendship` WRITE;
/*!40000 ALTER TABLE `friendship` DISABLE KEYS */;
/*!40000 ALTER TABLE `friendship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredient` (
                              `id` bigint NOT NULL AUTO_INCREMENT,
                              `name` varchar(255) DEFAULT NULL,
                              `id_owner` bigint NOT NULL,
                              PRIMARY KEY (`id`),
                              KEY `ingredient_users_null_fk` (`id_owner`),
                              CONSTRAINT `ingredient_users_null_fk` FOREIGN KEY (`id_owner`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
                                `id` bigint NOT NULL AUTO_INCREMENT,
                                `id_users` bigint NOT NULL,
                                `description` varchar(255) DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                KEY `notification_users_null_fk` (`id_users`),
                                CONSTRAINT `notification_users_null_fk` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,1,'Someone gave you a 5 stars note !');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe` (
                          `id` bigint NOT NULL AUTO_INCREMENT,
                          `id_owner` bigint NOT NULL,
                          `last_owner_visit_date` datetime DEFAULT NULL,
                          `name` varchar(255) DEFAULT NULL,
                          `creation_date` datetime DEFAULT NULL,
                          `sharing_mode` varchar(255) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          KEY `recipe_users_null_fk` (`id_owner`),
                          KEY `recipe_name_index` (`name`),
                          CONSTRAINT `recipe_users_null_fk` FOREIGN KEY (`id_owner`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,1,NULL,'super recette',NULL,NULL);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe_ingredient`
--

DROP TABLE IF EXISTS `recipe_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe_ingredient` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `id_recipe` bigint NOT NULL,
                                     `id_ingredient` bigint NOT NULL,
                                     `quantity` varchar(255) DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     KEY `recipe_ingredient_ingredient_null_fk` (`id_ingredient`),
                                     KEY `recipe_ingredient_recipe_null_fk` (`id_recipe`),
                                     CONSTRAINT `recipe_ingredient_ingredient_null_fk` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredient` (`id`),
                                     CONSTRAINT `recipe_ingredient_recipe_null_fk` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_ingredient`
--

LOCK TABLES `recipe_ingredient` WRITE;
/*!40000 ALTER TABLE `recipe_ingredient` DISABLE KEYS */;
/*!40000 ALTER TABLE `recipe_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe_note`
--

DROP TABLE IF EXISTS `recipe_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe_note` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `id_recipe` bigint NOT NULL,
                               `id_users` bigint NOT NULL,
                               `note` int DEFAULT NULL,
                               `favorite` boolean DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `recipe_note_recipe_null_fk` (`id_recipe`),
                               KEY `recipe_note_users_null_fk` (`id_users`),
                               CONSTRAINT `recipe_note_recipe_null_fk` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`),
                               CONSTRAINT `recipe_note_users_null_fk` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_note`
--

LOCK TABLES `recipe_note` WRITE;
/*!40000 ALTER TABLE `recipe_note` DISABLE KEYS */;
INSERT INTO `recipe_note` VALUES (1,1,1,3,NULL),(2,1,1,5,NULL);
/*!40000 ALTER TABLE `recipe_note` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;


--
-- Table structure for table `recipe_step`
--

DROP TABLE IF EXISTS `recipe_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe_step` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `id_recipe` bigint NOT NULL,
                               `position` int DEFAULT NULL,
                               `description` varchar(255) DEFAULT NULL,
                               `remark` varchar(255) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `recipe_step_recipe_null_fk` (`id_recipe`),
                               CONSTRAINT `recipe_step_recipe_null_fk` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_step`
--

LOCK TABLES `recipe_step` WRITE;
/*!40000 ALTER TABLE `recipe_step` DISABLE KEYS */;
/*!40000 ALTER TABLE `recipe_step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
                        `id` bigint NOT NULL AUTO_INCREMENT,
                        `name` varchar(255) DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shared_recipe`
--

DROP TABLE IF EXISTS `shared_recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shared_recipe` (
                                 `id` bigint NOT NULL AUTO_INCREMENT,
                                 `id_recipe` bigint NOT NULL,
                                 `id_friend` bigint NOT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `shared_recipe_recipe_null_fk` (`id_recipe`),
                                 KEY `shared_recipe_users_null_fk` (`id_friend`),
                                 CONSTRAINT `shared_recipe_recipe_null_fk` FOREIGN KEY (`id_recipe`) REFERENCES `recipe` (`id`),
                                 CONSTRAINT `shared_recipe_users_null_fk` FOREIGN KEY (`id_friend`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shared_recipe`
--

LOCK TABLES `shared_recipe` WRITE;
/*!40000 ALTER TABLE `shared_recipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `shared_recipe` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'buessard','matthieu','matbues@gmail.com','popsauce',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `users_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_role` (
                             `id` bigint NOT NULL AUTO_INCREMENT,
                             `id_users` bigint NOT NULL,
                             `id_role` bigint NOT NULL,
                             PRIMARY KEY (`id`),
                             KEY `users_role_role_null_fk` (`id_role`),
                             KEY `users_role_users_null_fk` (`id_users`),
                             CONSTRAINT `users_role_role_null_fk` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`),
                             CONSTRAINT `users_role_users_null_fk` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_role`
--

LOCK TABLES `users_role` WRITE;
/*!40000 ALTER TABLE `users_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_role` ENABLE KEYS */;
UNLOCK TABLES;

DROP USER IF EXISTS 'khizouser'@'localhost';

CREATE USER 'khizouser'@'localhost' IDENTIFIED BY 'a1ded7a09992ff9370a62fcef4987601';
GRANT SELECT, INSERT, UPDATE, DELETE ON users.* TO 'khizouser'@'localhost';

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

