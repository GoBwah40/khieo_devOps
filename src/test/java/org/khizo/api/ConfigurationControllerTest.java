package org.khizo.api;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.khizo.api.controller.ConfigurationController;
import org.khizo.api.model.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:test-datasource.properties")
class ConfigurationControllerTest {

    
    @Autowired
    private ConfigurationController configController;

    @BeforeEach
    void setUp() {
        
    }

    @Test
    void testGetAllConfiguration() {
        List<Configuration> clientList = new ArrayList<>();
        ResponseEntity<List<Configuration>> response = configController.getAllConfiguration();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clientList, response.getBody());
    }
}