package org.khizo.api;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.khizo.api.controller.*;
import org.khizo.api.model.*;
import org.khizo.api.model.security.RegisterDto;
import org.khizo.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations = "classpath:test-datasource.properties")
class SpecTest {

    @Autowired
    private UserController userController;
    @Autowired
    private RecipeController recipeController;
    @Autowired
    private RecipeStepController recipeStepController;
    @Autowired
    private RecipeNoteController recipeNoteController;
    @Autowired
    private RecipeNoteRepository recipeNoteRepository;
    @Autowired
    private RecipeStepRepository recipeStepRepository;
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LoginController loginController;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;


    @BeforeEach
    void setUp() {
        createRecipeAndUser();
    }

    @AfterEach
    void tearDown() {
        recipeNoteRepository.deleteAll();
        recipeStepRepository.deleteAll();
        notificationRepository.deleteAll();
        friendshipRepository.deleteAll();
        recipeRepository.deleteAll();
        userRoleRepository.deleteAll();
        userRepository.deleteAll();
        roleRepository.deleteAll();
    }
    private void createRecipeAndUser() {
        Users users = new Users(1L, "Test", "Man", "email@man.fr", "1234", null);
        userRepository.save(users);
        Role role = new Role(1L, RoleNameEnum.ADMIN);
        roleRepository.save(role);
        userRoleRepository.save(new UserRole(1L, users, role));
        Recipe recipe = new Recipe();
        recipe.setIdOwner(users);
        recipe.setName("Test");
        recipe.setRecipeId(1L);
        recipe.setSharingMode(SharingModeEnum.FRIENDS);
        recipeRepository.save(recipe);
        loginController.registerUser(new RegisterDto(users.getEmail(), "1234"));
    }

    /**
     * Stockage de recettes
     */
    @Test
    void testCreateRecipe() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        if (r.isPresent()) {
            Recipe recipe = r.get();
            Optional<Recipe> getRecipe = recipeController.getRecipeById(recipe.getRecipeId());
            getRecipe.ifPresent(value -> assertEquals(value.getRecipeId(), recipe.getRecipeId()));
            if (getRecipe.isPresent()) {
                assertEquals(getRecipe.get().getRecipeId(), recipe.getRecipeId());
                assertEquals(getRecipe.get().getName(), recipe.getName());
                assertEquals(getRecipe.get().getIdOwner().getUserId(), recipe.getIdOwner().getUserId());
            }
        }

    }

    /**
     * Suivi d'une recette
     */
    @Test
    void testAddNoteOrRemarkToRecipe() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        if (r.isPresent()){
            Recipe recipe = r.get();
            recipeController.createRecipe(recipe, 1L);
            recipeController.getCountRecipeStep(recipe.getRecipeId()).ifPresent(
                    value -> assertEquals(value, 0)
            );
            RecipeStep rs = new RecipeStep();
            rs.setPosition(1);
            rs.setRemark("Pas mal");
            rs.setDescription("Je suis content");
            rs.setIdRecipe(recipe);
            rs.setRecipeStepId(1L);
            recipeStepController.createRecipeStep(rs);
            Optional<RecipeStep> getRs = recipeStepController.getRecipeStepById(rs.getRecipeStepId());
            if (getRs.isPresent()) {
                assertEquals(getRs.get().getRemark(), rs.getRemark());
                assertEquals(getRs.get().getRecipeStepId(), rs.getRecipeStepId());
            }
            RecipeStep rs2 = new RecipeStep();
            rs2.setPosition(2);
            rs2.setRemark("Yes");
            rs2.setDescription("Je suis gentil");
            rs2.setIdRecipe(recipe);
            rs2.setRecipeStepId(2L);
            recipeStepController.createRecipeStep(rs2);
            assertEquals(recipeController.getAllRecipeStep(recipe.getRecipeId()).size(), 2);
            rs2.setDescription(rs.getDescription() + " et très beau");
            recipeStepController.updateRecipeStep(rs2.getRecipeStepId(), rs2);
            recipeStepController.getRecipeStepById(rs2.getRecipeStepId()).ifPresent(
                    value -> assertEquals(value.getDescription(), rs2.getDescription())
            );
        }
    }

    /**
     * Partage de recettes
     */
    @Test
    void testShareRecipe() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        if (r.isPresent()) {
            Recipe recipe = r.get();
            assertEquals(userController.getAllRecipeOfUser(1L).size(), 1);
            assertEquals(userController.getAllRecipeOfUser(1L).get(0).getName(), recipe.getName());
            recipeController.getRecipeById(recipe.getRecipeId()).ifPresent(
                    value -> assertEquals(value.getSharingMode().toString(), "FRIENDS")
            );
            recipe.setSharingMode(SharingModeEnum.ALL);
            recipeController.updateRecipe(recipe.getRecipeId(), recipe);
            recipeController.getRecipeById(recipe.getRecipeId()).ifPresent(
                    value -> assertEquals(value.getSharingMode(), recipe.getSharingMode())
            );
        }
    }

    /**
     * Recherche de recettes
     */
    @Test
    void testSearchByName() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Users user = u.get();
            Recipe recipe1 = new Recipe(2L, user, null, "Coucou", new Date(), SharingModeEnum.ALL);
            recipeController.createRecipe(recipe1, 1L);
            assertEquals(recipeController.getAllByName("cou").size(), 1);
            assertEquals(recipeController.getAllByName("Cou").size(), 1);
            assertEquals(recipeController.getAllByName("Piou").size(), 0);

        }
    }

    /**
     * Gestion des recettes favorites
     */
    @Test
    void testSetRecipeFavorite() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Recipe recipe = r.get();
            Users users = u.get();
            Recipe recipe2 = new Recipe(2L, users, null, "Test2", new Date(), SharingModeEnum.ALL);
            recipeController.createRecipe(recipe2, users.getUserId());

            RecipeNote rn = new RecipeNote();
            rn.setFavorite(false);
            rn.setRecipeNoteId(1L);
            rn.setNote(null);
            rn.setIdRecipe(recipe);
            rn.setIdUsers(users);
            recipeNoteController.createRecipeNote(rn);

            RecipeNote rn2 = new RecipeNote();
            rn2.setFavorite(false);
            rn2.setRecipeNoteId(2L);
            rn2.setNote(null);
            rn2.setIdRecipe(recipe2);
            rn2.setIdUsers(users);
            recipeNoteController.createRecipeNote(rn2);

            assertEquals(userController.getAllFavoriteRecipe(users.getUserId()).size(), 0);
            rn.setFavorite(true);
            recipeNoteController.updateRecipeNote(rn.getRecipeNoteId(), rn);
            assertEquals(userController.getAllFavoriteRecipe(users.getUserId()).size(), 1);
            assertEquals(recipeNoteController.getAllRecipeNote().size(), 2);

            Recipe recipe3 = new Recipe(3L, users, null, "Test3", new Date(), SharingModeEnum.ALL);
            recipeController.createRecipe(recipe3, users.getUserId());

            RecipeNote rn3 = new RecipeNote();
            rn3.setFavorite(false);
            rn3.setRecipeNoteId(3L);
            rn3.setNote(null);
            rn3.setIdRecipe(recipe3);
            rn3.setIdUsers(users);
            recipeNoteController.createRecipeNote(rn3);
            userController.setRecipeFavorite(recipe3.getRecipeId(), users.getUserId());
            assertEquals(userController.getAllFavoriteRecipe(users.getUserId()).size(), 2);
        }
    }

    /**
     * Notation des recettes
     */
    @Test
    void testNotation() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Recipe recipe = r.get();
            Users users = u.get();
            RecipeNote rn = new RecipeNote();
            rn.setFavorite(false);
            rn.setRecipeNoteId(1L);
            rn.setNote(null);
            rn.setIdRecipe(recipe);
            rn.setIdUsers(users);
            recipeNoteController.createRecipeNote(rn);
            recipeNoteController.getRecipeNoteById(rn.getRecipeNoteId()).ifPresent(
                    value -> assertNull(value.getNote())
            );
            assertEquals(userController.setNotationToRecipe(recipe.getRecipeId(), users.getUserId(), 3).getBody(), "Notation prise en compte.");
            recipeNoteController.getRecipeNoteById(rn.getRecipeNoteId()).ifPresent(
                    value -> assertEquals(value.getNote(), 3)
            );
        }
    }

    /**
     * Notifications pour les notes 5 étoiles
     */
    @Test
    void testNotification() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Recipe recipe = r.get();
            Users users = u.get();
            RecipeNote rn = new RecipeNote();
            rn.setFavorite(false);
            rn.setRecipeNoteId(1L);
            rn.setNote(null);
            rn.setIdRecipe(recipe);
            rn.setIdUsers(users);
            recipeNoteController.createRecipeNote(rn);
            recipeNoteController.getRecipeNoteById(rn.getRecipeNoteId()).ifPresent(
                    value -> assertNull(value.getNote())
            );
            assertEquals(userController.setNotationToRecipe(recipe.getRecipeId(), users.getUserId(), 5).getBody(), "Féléciation! Vous avez reçu une notation de 5 étoiles !");
            recipeNoteController.getRecipeNoteById(rn.getRecipeNoteId()).ifPresent(
                    value -> assertEquals(value.getNote(), 5)
            );
        }

    }

    /**
     * Suppression de recettes
     */
    @Test
    void testDeleteRecipe() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Recipe recipe = r.get();
            Users users = u.get();
            assertEquals(userController.getAllRecipeOfUser(users.getUserId()).size(), 1);
            assertEquals(userController.deleteRecipe(recipe.getRecipeId(), users.getUserId()).getBody(), "La recette a bien été supprimée.");
            assertEquals(userController.getAllRecipeOfUser(users.getUserId()).size(), 0);

            Recipe recipe2 = new Recipe(2L, users, null, "Test2", new Date(), SharingModeEnum.ALL);
            recipeController.createRecipe(recipe2, users.getUserId());
            Users users2 = new Users(2L, "Test2", "Girl", "email@girl.fr", "4321", null);
            userController.createUser(users2);
            userController.deleteRecipe(recipe2.getRecipeId(), users2.getUserId());
            assertEquals(userController.getAllRecipeOfUser(users.getUserId()).size(), 1);
            assertEquals(recipeController.getAllRecipe().size(), 1);
        }
    }

    /**
     * Modification de recettes
     */
    @Test
    void testUpdateRecipe() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> u = userController.getUserById(1L);
        if (r.isPresent() && u.isPresent()) {
            Recipe recipe = r.get();
            Users users = u.get();
            recipe.setName("Modifiée");
            assertEquals(userController.updateRecipe(recipe.getRecipeId(), users.getUserId(), recipe).getBody(), "La recette a bien été modifiée.");
            recipeController.getRecipeById(recipe.getRecipeId()).ifPresent(
                    value -> assertEquals(value.getName(), recipe.getName())
            );

            Recipe recipe2 = new Recipe(2L, users, null, "Test2", new Date(), SharingModeEnum.ALL);
            recipeController.createRecipe(recipe2, users.getUserId());
            Users users2 = new Users(2L, "Test2", "Girl", "email@girl.fr", "4321", null);
            userController.createUser(users2);
            recipe2.setName("Modifiée");
            assertEquals(userController.updateRecipe(recipe.getRecipeId(), users2.getUserId(), recipe).getBody(), "Vous n'avez pas les droit pour modifier cette recette.");
            recipeController.getRecipeById(recipe2.getRecipeId()).ifPresent(
                    value -> assertNotEquals(value.getName(), recipe.getName())
            );
        }

    }

    /**
     * Ajouter des amis
     */
    @Test
    void testAddFriends() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> uOptional = userController.getUserById(1L);
        if (r.isPresent() && uOptional.isPresent()) {
            Users users = uOptional.get();
            List<Users> list = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Users u = new Users((long) i, "test" + i, "Girl", "email@girl.fr", "4321", null);
                userController.createUser(u);
                list.add(u);
            }
            assertEquals(userController.getAllUser().size(), 10);
            for (Users u : list) {
                assertEquals(userController.AddFriend(users.getUserId(), u.getUserId()).getBody(), "Une nouvelle amité s'est créée avec " + u.getFirstname() + "!");
            }
            Users nope = new Users(40L, "test ultime", "Nope", "email@girl.fr", "4321", null);
            userController.createUser(nope);
            assertEquals(userController.AddFriend(users.getUserId(), nope.getUserId()).getBody(), "Impossible de créer une nouvelle amitié... ");
        }
       }

    /**
     * Supprimer des amis
     */
    @Test
    void testDeleteFriends() {
        Optional<Recipe> r = recipeController.getRecipeById(1L);
        Optional<Users> uOptional = userController.getUserById(1L);
        if (r.isPresent() && uOptional.isPresent()) {
            Recipe recipe = r.get();
            Users users = uOptional.get();
            assertEquals(userController.deleteFriend(users.getUserId(), 5L).getBody(), "Encore faut-il avoir des amis pour en supprimer.");
            List<Users> list = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Users u = new Users((long) i, "test" + i, "Girl", "email@girl.fr", "4321", null);
                userController.createUser(u);
                list.add(u);
                userController.AddFriend(users.getUserId(), u.getUserId());
            }
            List<Friendship> allFriendship = userController.getAllFriendsOfUser(users.getUserId());
            for (Friendship fs: allFriendship){
                userController.getUserById(fs.getIdUsers().getUserId()).ifPresent(
                        fs::setIdUsers
                );
                userController.getUserById(fs.getIdFriend().getUserId()).ifPresent(
                        fs::setIdFriend
                );
            }
            assertEquals(allFriendship.size(), list.size());
            assertEquals(userController.deleteFriend(users.getUserId(), 35L).getBody(), "Cette amitié n'a jamais existée.");
            assertEquals(userController.deleteFriend(users.getUserId(), allFriendship.get(4).getFriendshipId()).getBody(), "C'est la fin de l'amitié entre " + allFriendship.get(4).getIdFriend().getFirstname() + " et vous...");

        }
        }

}