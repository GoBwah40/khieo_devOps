package org.khizo.api.security;

import org.khizo.api.model.security.BearerToken;
import org.khizo.api.model.security.LoginDto;
import org.khizo.api.model.security.RegisterDto;
import org.springframework.http.ResponseEntity;

public interface IAppUserSecurityService {

    ResponseEntity<?> register(RegisterDto rRegisterDto);

    BearerToken authenticate(LoginDto rLoginDto);
}
