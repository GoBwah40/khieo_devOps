package org.khizo.api.security;

import org.khizo.api.model.Users;
import org.khizo.api.model.UserRole;
import org.khizo.api.model.security.BearerToken;
import org.khizo.api.model.security.LoginDto;
import org.khizo.api.model.security.RegisterDto;
import org.khizo.api.repository.IUserRepository;
import org.khizo.api.repository.UserRepository;
import org.khizo.api.repository.UserRoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AppUserSecurityService implements IAppUserSecurityService {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtilities jwtUtilities;

    private final IUserRepository iUserRepository;

    private static final String TOKEN_TYPE = "Bearer";

    public AppUserSecurityService(UserRoleRepository userRoleRepository, AuthenticationManager authenticationManager, UserRepository memberRepository, PasswordEncoder passwordEncoder, JwtUtilities jwtUtilities, IUserRepository iUserRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = memberRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtilities = jwtUtilities;
        this.userRoleRepository = userRoleRepository;
        this.iUserRepository = iUserRepository;
    }

    @Override
    public ResponseEntity<?> register(RegisterDto rRegisterDto) {

        //verifie si email existe deja dans la base
        if (iUserRepository.existsByEmail(rRegisterDto.getEmail())) {
            return new ResponseEntity<>("Email deja pris !", HttpStatus.BAD_REQUEST);
        }

        //creation du user en base
        Users users = new Users();
        users.setEmail(rRegisterDto.getEmail());
        users.setPassword(passwordEncoder.encode(rRegisterDto.getPassword()));

        //role admin pour le user
        List<UserRole> userRoles = userRoleRepository.findByUserId(users.getUserId()).get();

        //Sauvegarde le user
        iUserRepository.save(users);

        //creation du token
        String token = jwtUtilities.generateToken(rRegisterDto.getEmail(), Collections.singletonList(userRoles.get(0).getRole().getRoleName().toString()));
        //renvoi du token dans la reponse
        return new ResponseEntity<>(new BearerToken(token, TOKEN_TYPE), HttpStatus.OK);
    }

    @Override
    public BearerToken authenticate(LoginDto rLoginDto) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                rLoginDto.getEmail(), rLoginDto.getPassword());
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Users users = userRepository.findByEmail(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        List<String> rolesNames = new ArrayList<>();
        List<UserRole> userRole = userRoleRepository.findByUserId(users.getUserId())
                .orElseThrow(() -> new UsernameNotFoundException("Role not found"));
        userRole.forEach(
                ur -> rolesNames.add(ur.getRole().getRoleName().toString())
        );
        return new BearerToken(jwtUtilities.generateToken(users.getEmail(), rolesNames), TOKEN_TYPE);
    }

}
