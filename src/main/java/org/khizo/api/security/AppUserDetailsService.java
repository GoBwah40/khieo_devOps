package org.khizo.api.security;

import lombok.RequiredArgsConstructor;
import org.khizo.api.model.Users;
import org.khizo.api.model.UserRole;
import org.khizo.api.repository.RoleRepository;
import org.khizo.api.repository.UserRepository;
import org.khizo.api.repository.UserRoleRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AppUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Users users = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException("Utilisateur inconnu : " + email));

        List<UserRole> userRoles = userRoleRepository.findByUserId(users.getUserId())
                .orElseThrow(() ->
                        new UsernameNotFoundException("Role inconnu"));

        Set<GrantedAuthority> authorities = userRoles.stream().map(
                (ur) -> new SimpleGrantedAuthority(roleRepository.findByRoleId(ur.getRole()).getRoleName().toString())).collect(Collectors.toSet());

        return new org.springframework.security.core.userdetails.User(users.getEmail(),
                users.getPassword(),
                authorities);
    }

}