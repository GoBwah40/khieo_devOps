package org.khizo.api.repository;

import org.khizo.api.model.Friendship;
import org.khizo.api.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Long> {

    List<Friendship> findByIdUsersAndIdFriend(Users sender, Users receiver);
    List<Friendship> findAllByIdUsers(Users users);
    Integer countByIdUsers(Users sender);
}