package org.khizo.api.repository;

import jakarta.validation.constraints.NotNull;
import org.khizo.api.model.Recipe;
import org.khizo.api.model.RecipeStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeStepRepository extends JpaRepository<RecipeStep, Long> {
    Optional<Integer> countByIdRecipe(@NotNull Recipe idRecipe);

    List<RecipeStep> findAllByIdRecipe(@NotNull Recipe idRecipe);

}