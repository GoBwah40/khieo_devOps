package org.khizo.api.repository;

import org.khizo.api.model.Role;
import org.khizo.api.model.RoleNameEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleName(RoleNameEnum roleName);

    Role findByRoleId(Role idRole);
}