package org.khizo.api.repository;

import org.khizo.api.model.Recipe;
import org.khizo.api.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findAllByIdOwner(Users users);

    List<Recipe> findAllByNameContainingIgnoreCase(String name);

}