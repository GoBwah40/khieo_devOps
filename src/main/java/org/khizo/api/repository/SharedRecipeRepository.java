package org.khizo.api.repository;

import org.khizo.api.model.SharedRecipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SharedRecipeRepository extends JpaRepository<SharedRecipe, Long> {
}