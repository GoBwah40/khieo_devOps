package org.khizo.api.repository;

import org.khizo.api.model.Recipe;
import org.khizo.api.model.RecipeNote;
import org.khizo.api.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeNoteRepository extends JpaRepository<RecipeNote, Long> {

    List<RecipeNote> findAllByIdUsersAndFavoriteIsTrue(Users users);

    Optional<RecipeNote> findAllByIdUsersAndIdRecipe(Users users, Recipe recipe);
}