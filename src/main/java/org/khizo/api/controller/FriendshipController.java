package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Friendship;
import org.khizo.api.service.FriendshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/friendship", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Friendship", description = "Endpoints for friendship")
@Controller
public class FriendshipController {

    @Autowired
    private FriendshipService friendshipService;

    @GetMapping("/all")
    public List<Friendship> getAllFriendship() {
        return friendshipService.getAllFriendship();
    }

    @GetMapping("/{id}")
    public Optional<Friendship> getFriendshipById(@PathVariable(name = "id") final Long id) {
        return friendshipService.getFriendshipById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateFriendship(@PathVariable(name = "id") final Integer id,
                                                       @RequestBody @Valid final Friendship f) {
        friendshipService.updateFriendship(f, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteFriendship(@PathVariable(name = "id") final Integer id) {
        friendshipService.deleteFriendship(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createFriendship(@RequestBody @Valid final Friendship f) {
        final Integer createdId = friendshipService.createFriendship(f);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @DeleteMapping("/all")
    @ApiResponse(responseCode = "204")
    public void deleteAllFriendship() {
        friendshipService.deleteAllFriendship();
    }
}
