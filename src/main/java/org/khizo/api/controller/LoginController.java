package org.khizo.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.khizo.api.model.security.BearerToken;
import org.khizo.api.model.security.LoginDto;
import org.khizo.api.model.security.RegisterDto;
import org.khizo.api.security.IAppUserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class LoginController {

    @Autowired
    private IAppUserSecurityService appUserSecurityService;

    @PostMapping("/signin")
    public BearerToken authenticateUser(@RequestBody LoginDto rLoginDto) {
        return appUserSecurityService.authenticate(rLoginDto);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody RegisterDto rRegisterDto) {
        return appUserSecurityService.register(rRegisterDto);
    }
}
