package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.RecipeStep;
import org.khizo.api.service.RecipeStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/recipeStep", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Recipe step", description = "Endpoints for Recipe step")
@Controller
public class RecipeStepController {

    @Autowired
    private RecipeStepService recipeStepService;

    @GetMapping("/all")
    public List<RecipeStep> getAllRecipeStep() {
        return recipeStepService.getAllRecipeStep();
    }

    @GetMapping("/{id}")
    public Optional<RecipeStep> getRecipeStepById(@PathVariable(name = "id") final Long id) {
        return recipeStepService.getRecipeStepById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Long> updateRecipeStep(@PathVariable(name = "id") final Long id,
                                                    @RequestBody @Valid final RecipeStep rs) {
        recipeStepService.updateRecipeStep(rs, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteRecipeStep(@PathVariable(name = "id") final Integer id) {
        recipeStepService.deleteRecipeStep(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRecipeStep(@RequestBody @Valid final RecipeStep rs) {
        final Integer createdId = recipeStepService.createRecipeStep(rs);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @DeleteMapping("/all")
    @ApiResponse(responseCode = "204")
    public void deleteAllRecipeNote() {
        recipeStepService.deleteAllRecipeStep();
    }

}
