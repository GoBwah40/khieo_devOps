package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.RecipeNote;
import org.khizo.api.service.RecipeNoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/recipeNote", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Recipe note", description = "Endpoints for recipe note")
@Controller
public class RecipeNoteController {

    @Autowired
    private RecipeNoteService recipeNoteService;

    @GetMapping("/all")
    public List<RecipeNote> getAllRecipeNote() {
        return recipeNoteService.getAllRecipeNote();
    }

    @GetMapping("/{id}")
    public Optional<RecipeNote> getRecipeNoteById(@PathVariable(name = "id") final Long id) {
        return recipeNoteService.getRecipeNoteById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Long> updateRecipeNote(@PathVariable(name = "id") final Long id, @RequestBody @Valid final RecipeNote rn) {
        recipeNoteService.updateRecipeNote(rn, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteRecipeNote(@PathVariable(name = "id") final Integer id) {
        recipeNoteService.deleteRecipeNote(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRecipeNote(@RequestBody @Valid final RecipeNote rn) {
        final Integer createdId = recipeNoteService.createRecipeNote(rn);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
    @DeleteMapping("/all")
    @ApiResponse(responseCode = "204")
    public void deleteAllRecipeNote() {
        recipeNoteService.deleteAllRecipeNote();
    }
}
