package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Notification;
import org.khizo.api.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/notification", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Notification", description = "Endpoints for notification")
@Controller
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @GetMapping("/all")
    public List<Notification> getAllNotification() {
        return notificationService.getAllNotification();
    }

    @GetMapping("/{id}")
    public Optional<Notification> getNotificationById(@PathVariable(name = "id") final Integer id) {
        return notificationService.getNotificationById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateNotification(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final Notification n) {
        notificationService.updateNotification(n, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteNotification(@PathVariable(name = "id") final Integer id) {
        notificationService.deleteNotification(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createNotification(@RequestBody @Valid final Notification n) {
        final Integer createdId = notificationService.createNotification(n);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }


}
