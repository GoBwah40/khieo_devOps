package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Configuration;
import org.khizo.api.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/accommodation", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Configuration", description = "Endpoints for configuration")
@Controller
public class ConfigurationController {

    @Autowired
    private ConfigurationService configurationService;

    @GetMapping("/all")
    public ResponseEntity<List<Configuration>> getAllConfiguration() {
        return ResponseEntity.ok(configurationService.getAllConfiguration());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Configuration>> getConfigurationById(@PathVariable(name = "id") final Integer id) {
        return ResponseEntity.ok(configurationService.getConfigurationById(id));

    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateConfiguration(@PathVariable(name = "id") final Integer id,
                                                       @RequestBody @Valid final Configuration conf) {
        configurationService.updateConfiguration(conf, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public ResponseEntity<Integer> deleteConfiguration(@PathVariable(name = "id") final Integer id) {
        return new ResponseEntity<>(id, HttpStatus.NO_CONTENT);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Long> createConfiguration(@RequestBody @Valid final Configuration conf) {
        final Long createdId = configurationService.createConfiguration(conf);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

}
