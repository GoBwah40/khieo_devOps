package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.RecipeIngredient;
import org.khizo.api.service.RecipeIngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/recipeIngredient", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Recipe ingredient", description = "Endpoints for recipe ingredient")
@Controller
public class RecipeIngredientController {

    @Autowired
    private RecipeIngredientService recipeIngredientService;

    @GetMapping("/all")
    public List<RecipeIngredient> getAllRecipeIngredient() {
        return recipeIngredientService.getAllRecipeIngredient();
    }

    @GetMapping("/{id}")
    public Optional<RecipeIngredient> getRecipeIngredientById(@PathVariable(name = "id") final Integer id) {
        return recipeIngredientService.getRecipeIngredientById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateRecipeIngredient(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final RecipeIngredient ri) {
        recipeIngredientService.updateRecipeIngredient(ri, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteRecipeIngredient(@PathVariable(name = "id") final Integer id) {
        recipeIngredientService.deleteRecipeIngredient(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRecipeIngredient(@RequestBody @Valid final RecipeIngredient ri) {
        final Integer createdId = recipeIngredientService.createRecipeIngredient(ri);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
}
