package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Recipe;
import org.khizo.api.model.RecipeStep;
import org.khizo.api.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/recipe", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Recipe", description = "Endpoints for recipe")
@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/all")
    public List<Recipe> getAllRecipe() {
        return recipeService.getAllRecipe();
    }

    @GetMapping("/{id}")
    public Optional<Recipe> getRecipeById(@PathVariable(name = "id") final Long id) {
        return recipeService.getRecipeById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Long> updateRecipe(@PathVariable(name = "id") final Long id, @RequestBody @Valid final Recipe r) {
        recipeService.updateRecipe(r, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteRecipe(@PathVariable(name = "id") final Integer id) {
        recipeService.deleteRecipe(id);
    }

    @PostMapping("/new")
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRecipe(@RequestBody @Valid final Recipe r, final Long userId) {
        final Integer createdId = recipeService.createRecipe(r, userId);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @GetMapping("/{id}/count")
    public Optional<Integer> getCountRecipeStep(@PathVariable(name = "id") final Long id) {
        return recipeService.getCountRecipeStep(id);
    }

    @GetMapping("/{id}/allStep")
    public List<RecipeStep> getAllRecipeStep(@PathVariable(name = "id") final Long id) {
        return recipeService.getAllRecipeStep(id);
    }

    @GetMapping("/{name}/all")
    public List<Recipe> getAllByName(@PathVariable(name = "name") final String name) {
        return recipeService.getRecipeByName(name);
    }

    @DeleteMapping("/all")
    @ApiResponse(responseCode = "204")
    public void deleteAllRecipe() {
        recipeService.deleteAllRecipe();
    }
}
