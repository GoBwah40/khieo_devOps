package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Ingredient;
import org.khizo.api.model.Users;
import org.khizo.api.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/ingredient", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Ingredient", description = "Endpoints for ingredient")
@Controller
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @GetMapping("/all")
    public List<Ingredient> getAllFriendship() {
        return ingredientService.getAllIngredient();
    }

    @GetMapping("/{id}")
    public Optional<Ingredient> getFriendshipById(@PathVariable(name = "id") final Integer id) {
        return ingredientService.getIngredientById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateIngredient(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final Ingredient i,
                                                    final Users users) {
        ingredientService.updateIngredient(i, id, users);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteIngredient(@PathVariable(name = "id") final Integer id) {
        ingredientService.deleteIngredient(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createIngredient(@RequestBody @Valid final Ingredient i, Users users) {
        final Integer createdId = ingredientService.createIngredient(i, users);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }


}
