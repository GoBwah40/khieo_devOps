package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Friendship;
import org.khizo.api.model.Recipe;
import org.khizo.api.model.RecipeNote;
import org.khizo.api.model.Users;
import org.khizo.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "User", description = "Endpoints for user")
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public List<Users> getAllUser() {
        return userService.getAllUser();
    }

    @GetMapping("/{id}")
    public Optional<Users> getUserById(@PathVariable(name = "id") final Long id) {
        return userService.getUserById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Users> updateUser(@PathVariable(name = "id") final Long id,
                                            @RequestBody @Valid final Users u) {
        return ResponseEntity.ok(userService.updateUser(u, id));
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteUser(@PathVariable(name = "id") final Long id) {
        userService.deleteUser(id);
    }

    @PostMapping("/create")
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createUser(@RequestBody @Valid final Users u) {
        final Integer createdId = userService.createUser(u);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @GetMapping("/allRecipe/{idUser}")
    public List<Recipe> getAllRecipeOfUser(@PathVariable(name = "idUser") final Long idUser) {
        return userService.getAllRecipe(idUser);
    }

    @GetMapping("/recipe/favorite")
    public List<Recipe> getAllFavoriteRecipe(final Long id) {
        return userService.getAllFavoriteRecipe(id);
    }

    @GetMapping("/recipe/{idRecipe}/setFavorite")
    public RecipeNote setRecipeFavorite(@PathVariable(name = "idRecipe") final Long idRecipe, final Long idUser) {
        return userService.setRecipeFavorite(idRecipe, idUser);
    }

    @GetMapping("/recipe/{idRecipe}/setNotation")
    public ResponseEntity<String> setNotationToRecipe(@PathVariable(name = "idRecipe") final Long idRecipe, final Long idUser, final Integer notation) {
        return ResponseEntity.ok(userService.setNotationToRecipe(idRecipe, idUser, notation));
    }

    @DeleteMapping("/recipe/{idRecipe}/delete")
    @ApiResponse(responseCode = "204")
    public ResponseEntity<String> deleteRecipe(@PathVariable(name = "idRecipe") final Long idRecipe, final Long idUser) {
        return ResponseEntity.ok(userService.deleteRecipe(idRecipe, idUser));
    }

    @PostMapping("/recipe/{idRecipe}/update")
    public ResponseEntity<String> updateRecipe(@PathVariable(name = "idRecipe") final Long idRecipe,
                                           final Long userId,
                                           @RequestBody @Valid final Recipe recipe) {
        return ResponseEntity.ok(userService.updateRecipe(idRecipe, userId, recipe));
    }

    @GetMapping("/friend/add")
    public ResponseEntity<String> AddFriend(final Long idUser, final Long idFriend) {
        return ResponseEntity.ok(userService.addFriend(idUser, idFriend));
    }

    @GetMapping("/friend/all")
    public List<Friendship> getAllFriendsOfUser(final Long idUser) {
        return userService.getAllFriendsOfUser(idUser);
    }

    @DeleteMapping("/friends/delete")
    @ApiResponse(responseCode = "204")
    public ResponseEntity<String> deleteFriend(final Long idUser, final Long idFriendship) {
        return ResponseEntity.ok(userService.deleteFriend(idUser, idFriendship));
    }

    @DeleteMapping("/all")
    @ApiResponse(responseCode = "204")
    public void deleteAllUser() {
        userService.deleteAllUser();
    }

    @PostMapping("/recipe/create")
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRecipe(@RequestBody @Valid final Recipe r, final Long idUser) {
        final Integer createdId = userService.createRecipe(r, idUser);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
}
