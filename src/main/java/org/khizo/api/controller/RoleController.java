package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.Role;
import org.khizo.api.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/role", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Role", description = "Endpoints for role")
@Controller
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/all")
    public List<Role> getAllRole() {
        return roleService.getAllRole();
    }

    @GetMapping("/{id}")
    public Optional<Role> getRoleById(@PathVariable(name = "id") final Integer id) {
        return roleService.getRoleById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateRole(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final Role r) {
        roleService.updateRole(r, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteRole(@PathVariable(name = "id") final Integer id) {
        roleService.deleteRole(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createRole(@RequestBody @Valid final Role r) {
        final Integer createdId = roleService.createRole(r);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
}
