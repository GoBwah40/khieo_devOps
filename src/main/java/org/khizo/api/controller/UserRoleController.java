package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.UserRole;
import org.khizo.api.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/userRole", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "User role", description = "Endpoints for user role")
@Controller
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @GetMapping("/all")
    public List<UserRole> getAllUserRole() {
        return userRoleService.getAllUserRole();
    }

    @GetMapping("/{id}")
    public Optional<UserRole> getUserRoleById(@PathVariable(name = "id") final Integer id) {
        return userRoleService.getUserRoleById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateUserRole(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final UserRole ur) {
        userRoleService.updateUserRole(ur, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteUserRole(@PathVariable(name = "id") final Integer id) {
        userRoleService.deleteUserRole(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createUserRole(@RequestBody @Valid final UserRole ur) {
        final Integer createdId = userRoleService.createUserRole(ur);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
}
