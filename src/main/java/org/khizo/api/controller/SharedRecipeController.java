package org.khizo.api.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.khizo.api.model.SharedRecipe;
import org.khizo.api.service.SharedRecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/sharedRecipeController", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@Tag(name = "Shared recipe", description = "Endpoints for Shared recipe")
@Controller
public class SharedRecipeController {

    @Autowired
    private SharedRecipeService sharedRecipeService;

    @GetMapping("/all")
    public List<SharedRecipe> getAllSharedRecipe() {
        return sharedRecipeService.getAllSharedRecipe();
    }

    @GetMapping("/{id}")
    public Optional<SharedRecipe> getSharedRecipeById(@PathVariable(name = "id") final Integer id) {
        return sharedRecipeService.getSharedRecipeById(id);
    }

    @PostMapping("/{id}/update")
    public ResponseEntity<Integer> updateSharedRecipe(@PathVariable(name = "id") final Integer id,
                                                    @RequestBody @Valid final SharedRecipe sr) {
        sharedRecipeService.updateSharedRecipe(sr, id);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public void deleteSharedRecipe(@PathVariable(name = "id") final Integer id) {
        sharedRecipeService.deleteSharedRecipe(id);
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<Integer> createSharedRecipe(@RequestBody @Valid final SharedRecipe sr) {
        final Integer createdId = sharedRecipeService.createSharedRecipe(sr);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }
}
