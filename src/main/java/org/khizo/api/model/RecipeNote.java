package org.khizo.api.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "recipe_note", schema = "khizo")
public class RecipeNote {
    @Id
    @Column(name = "id", nullable = false)
    private Long recipeNoteId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_recipe", nullable = false)
    private Recipe idRecipe;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_users", nullable = false)
    private Users idUsers;

    @Column(name = "note")
    private Integer note;

    @Column(name = "favorite")
    private Boolean favorite;

}