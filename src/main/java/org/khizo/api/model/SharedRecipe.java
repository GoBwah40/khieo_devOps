package org.khizo.api.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "shared_recipe", schema = "khizo")
public class SharedRecipe {
    @Id
    @Column(name = "id", nullable = false)
    private Long sharedRecipeId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_recipe", nullable = false)
    private Recipe idRecipe;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_friend", nullable = false)
    private Users idFriend;

}