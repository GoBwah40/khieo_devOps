package org.khizo.api.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "recipe", schema = "khizo")
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long recipeId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_owner", nullable = false)
    private Users idOwner;

    @Column(name = "last_owner_visit_date")
    private Date lastOwnerVisitDate;

    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @Column(name = "creation_date")
    private Date creationDate;

    @Size(max = 255)
    @Column(name = "sharing_mode")
    private SharingModeEnum sharingMode;

}