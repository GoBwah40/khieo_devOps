package org.khizo.api.model;

public enum SharingModeEnum {
    ALL, FRIENDS, PRIVATE
}
