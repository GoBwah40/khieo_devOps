package org.khizo.api.service;

import org.khizo.api.model.UserRole;
import org.khizo.api.repository.UserRoleRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public List<UserRole> getAllUserRole() {
        return userRoleRepository.findAll();
    }

    public Optional<UserRole> getUserRoleById(int id) {
        return userRoleRepository.findById((long) id);
    }

    public void updateUserRole(UserRole ur, int id) {
        final UserRole userRole = userRoleRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        userRoleRepository.save(mapToEntity(ur, userRole));
    }

    private UserRole mapToEntity(final UserRole ur,
                                   final UserRole userRole) {
        userRole.setRole(ur.getRole());
        userRole.setUserRoleId(ur.getUserRoleId());
        userRole.setUsers(ur.getUsers());
        return userRole;
    }
    public Integer createUserRole(final UserRole ur) {
        final UserRole userRole = new UserRole();
        mapToEntity(ur, userRole);
        return Math.toIntExact(userRoleRepository.save(userRole).getUserRoleId());
    }

    public void deleteUserRole(Integer id) {
        userRoleRepository.deleteById(Long.valueOf(id));
    }

}
