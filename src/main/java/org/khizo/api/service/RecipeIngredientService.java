package org.khizo.api.service;

import org.khizo.api.model.RecipeIngredient;
import org.khizo.api.repository.RecipeIngredientRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeIngredientService {

    @Autowired
    private RecipeIngredientRepository recipeIngredientRepository;

    public List<RecipeIngredient> getAllRecipeIngredient() {
        return recipeIngredientRepository.findAll();
    }

    public Optional<RecipeIngredient> getRecipeIngredientById(int id) {
        return recipeIngredientRepository.findById((long) id);
    }

    public void updateRecipeIngredient(RecipeIngredient ri, int id) {
        final RecipeIngredient recipeIngredient = recipeIngredientRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        recipeIngredientRepository.save(mapToEntity(ri, recipeIngredient));
    }

    private RecipeIngredient mapToEntity(final RecipeIngredient ri,
                                     final RecipeIngredient recipeIngredient) {
        recipeIngredient.setIdIngredient(ri.getIdIngredient());
        recipeIngredient.setIdRecipe(ri.getIdRecipe());
        recipeIngredient.setQuantity(ri.getQuantity());
        recipeIngredient.setRecipeIngredientId(ri.getRecipeIngredientId());
        return recipeIngredient;
    }

    public Integer createRecipeIngredient(final RecipeIngredient ri) {
        final RecipeIngredient recipeIngredient = new RecipeIngredient();
        mapToEntity(ri, recipeIngredient);
        return Math.toIntExact(recipeIngredientRepository.save(recipeIngredient).getRecipeIngredientId());
    }

    public void deleteRecipeIngredient(Integer id) {
        recipeIngredientRepository.deleteById(Long.valueOf(id));
    }
}
