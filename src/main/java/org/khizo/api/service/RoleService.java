package org.khizo.api.service;

import org.khizo.api.model.Role;
import org.khizo.api.repository.RoleRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getAllRole() {
        return roleRepository.findAll();
    }

    public Optional<Role> getRoleById(int id) {
        return roleRepository.findById((long) id);
    }

    public void updateRole(Role r, int id) {
        final Role role = roleRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        roleRepository.save(mapToEntity(r, role));
    }

    private Role mapToEntity(final Role r,
                                     final Role role) {
        role.setRoleId(r.getRoleId());
        role.setRoleName(r.getRoleName());
        return role;
    }

    public Integer createRole(final Role r) {
        final Role role = new Role();
        mapToEntity(r, role);
        return Math.toIntExact(roleRepository.save(role).getRoleId());
    }

    public void deleteRole(Integer id) {
        roleRepository.deleteById(Long.valueOf(id));
    }
}
