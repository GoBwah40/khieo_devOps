package org.khizo.api.service;

import org.khizo.api.model.RecipeStep;
import org.khizo.api.repository.RecipeStepRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeStepService {

    @Autowired
    private RecipeStepRepository recipeStepRepository;
    public List<RecipeStep> getAllRecipeStep() {
        return recipeStepRepository.findAll();
    }

    public Optional<RecipeStep> getRecipeStepById(Long id) {
        return recipeStepRepository.findById(id);
    }

    public void updateRecipeStep(final RecipeStep rs, final Long id) {
        final RecipeStep recipeStep = recipeStepRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        recipeStepRepository.save(mapToEntity(rs, recipeStep));
    }

    private RecipeStep mapToEntity(final RecipeStep ns,
                                     final RecipeStep recipeStep) {
        recipeStep.setRecipeStepId(ns.getRecipeStepId());
        recipeStep.setIdRecipe(ns.getIdRecipe());
        recipeStep.setDescription(ns.getDescription());
        recipeStep.setRemark(ns.getRemark());
        recipeStep.setPosition(ns.getPosition());
        return recipeStep;
    }

    public Integer createRecipeStep(final RecipeStep rs) {
        final RecipeStep recipeStep = new RecipeStep();
        mapToEntity(rs, recipeStep);
        return Math.toIntExact(recipeStepRepository.save(recipeStep).getRecipeStepId());
    }

    public void deleteRecipeStep(Integer id) {
        recipeStepRepository.deleteById(Long.valueOf(id));
    }

    public void deleteAllRecipeStep() {
        recipeStepRepository.deleteAll();
    }
}
