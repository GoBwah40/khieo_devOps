package org.khizo.api.service;

import org.khizo.api.model.Recipe;
import org.khizo.api.model.RecipeStep;
import org.khizo.api.model.Users;
import org.khizo.api.repository.RecipeRepository;
import org.khizo.api.repository.RecipeStepRepository;
import org.khizo.api.repository.UserRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeStepRepository recipeStepRepository;
    @Autowired
    private UserRepository userRepository;

    public List<Recipe> getAllRecipe() {
        return recipeRepository.findAll();
    }

    public Optional<Recipe> getRecipeById(Long id) {
        return recipeRepository.findById(id);
    }

    public void updateRecipe(Recipe r, Long id) {
        final Recipe recipe = recipeRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        recipeRepository.save(mapToEntity(r, recipe, null));
    }

    private Recipe mapToEntity(final Recipe r, final Recipe recipe, final Users users) {
        recipe.setName(r.getName());
        recipe.setCreationDate(r.getCreationDate());
        recipe.setSharingMode(r.getSharingMode());
        recipe.setLastOwnerVisitDate(r.getLastOwnerVisitDate());
        if (users != null){
            recipe.setIdOwner(users);
        }
        return recipe;
    }

    public Integer createRecipe(final Recipe r, final Long idUser) {
        final Recipe recipe = new Recipe();
        final Optional<Users> user = userRepository.findById(idUser);
        user.ifPresent(value -> mapToEntity(r, recipe, value));
        return Math.toIntExact(recipeRepository.save(recipe).getRecipeId());
    }

    public void deleteRecipe(Integer id) {
        recipeRepository.deleteById(Long.valueOf(id));
    }

    public Optional<Integer> getCountRecipeStep(Long recipeId) {
        Recipe recipe = recipeRepository.findById(recipeId)
                .orElseThrow(NotFoundException::new);
        return recipeStepRepository.countByIdRecipe(recipe);
    }

    public List<RecipeStep> getAllRecipeStep(Long recipeId) {
        Recipe recipe = recipeRepository.findById(recipeId)
                .orElseThrow(NotFoundException::new);
        return recipeStepRepository.findAllByIdRecipe(recipe);
    }

    public List<Recipe> getRecipeByName(String name) {
        return recipeRepository.findAllByNameContainingIgnoreCase(name);
    }

    public void deleteAllRecipe() {
        recipeRepository.deleteAll();
    }
}
