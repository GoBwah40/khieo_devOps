package org.khizo.api.service;

import org.khizo.api.model.Ingredient;
import org.khizo.api.model.Users;
import org.khizo.api.repository.IngredientRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    public List<Ingredient> getAllIngredient() {
        return ingredientRepository.findAll();
    }

    public Optional<Ingredient> getIngredientById(int id) {
        return ingredientRepository.findById((long) id);
    }

    public void updateIngredient(final Ingredient i, final int id, final Users users) {
        final Ingredient ingredient = ingredientRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        ingredient.setIdOwner(users);
        ingredientRepository.save(mapToEntity(i, ingredient));
    }

    private Ingredient mapToEntity(final Ingredient i,
                                      final Ingredient ingredient) {
        ingredient.setName(i.getName());
        ingredient.setIngredientId(i.getIngredientId());
        return ingredient;
    }

    public Integer createIngredient(final Ingredient i, final Users users) {
        final Ingredient ingredient = new Ingredient();
        ingredient.setIdOwner(users);
        mapToEntity(i, ingredient);
        return Math.toIntExact(ingredientRepository.save(ingredient).getIngredientId());
    }

    public void deleteIngredient(Integer id) {
        ingredientRepository.deleteById(Long.valueOf(id));
    }
}
