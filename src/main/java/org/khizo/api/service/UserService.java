package org.khizo.api.service;

import org.khizo.api.model.*;
import org.khizo.api.repository.*;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeNoteRepository recipeNoteRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;

    public List<Users> getAllUser() {
        return userRepository.findAll();
    }

    public Optional<Users> getUserById(Long id) {
        return userRepository.findById(id);
    }

    public Users updateUser(final Users u, final Long id) {
        final Users users = userRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return userRepository.save(mapToEntity(u, users));
    }

    private Users mapToEntity(final Users u,
                              final Users users) {
        users.setUserId(u.getUserId());
        users.setBanDate(u.getBanDate());
        users.setEmail(u.getEmail());
        users.setLastname(u.getLastname());
        users.setFirstname(u.getFirstname());
        users.setPassword(u.getPassword());
        return users;
    }

    public Integer createUser(final Users u) {
        final Users users = new Users();
        mapToEntity(u, users);
        return Math.toIntExact(userRepository.save(users).getUserId());
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public List<Recipe> getAllRecipe(Long id) {
        Users users = userRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return recipeRepository.findAllByIdOwner(users);
    }

    public List<Recipe> getAllFavoriteRecipe(Long id) {
        Users users = userRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        List<RecipeNote> list = recipeNoteRepository.findAllByIdUsersAndFavoriteIsTrue(users);
        List<Recipe> favoriteRecipes = new ArrayList<>();
        for (RecipeNote rn : list) {
            Recipe recipe = recipeRepository.findById(rn.getIdRecipe().getRecipeId())
                    .orElseThrow(NotFoundException::new);
            favoriteRecipes.add(recipe);
        }
        return favoriteRecipes;
    }

    public RecipeNote setRecipeFavorite(Long idRecipe, Long idUser) {
        Users users = userRepository.findById(idUser)
                .orElseThrow(NotFoundException::new);
        Recipe recipe = recipeRepository.findById(idRecipe)
                .orElseThrow(NotFoundException::new);
        RecipeNote rn = recipeNoteRepository.findAllByIdUsersAndIdRecipe(users, recipe)
                .orElseThrow(NotFoundException::new);
        if (rn.getFavorite()) {
            throw new IllegalStateException("Recipe already marked as favorite");
        }
        rn.setFavorite(true);
        return recipeNoteRepository.save(rn);
    }

    public String setNotationToRecipe(Long idRecipe, Long idUser, Integer notation) {
        Users users = userRepository.findById(idUser)
                .orElseThrow(NotFoundException::new);
        Recipe recipe = recipeRepository.findById(idRecipe)
                .orElseThrow(NotFoundException::new);
        RecipeNote rn = recipeNoteRepository.findAllByIdUsersAndIdRecipe(users, recipe)
                .orElseThrow(NotFoundException::new);
        rn.setNote(notation);
        if (notation == 5) {
            return setMaxNotationToRecipe(rn, recipe).getDescription();
        } else {
            recipeNoteRepository.save(rn);
            return "Notation prise en compte.";
        }
    }

    public Notification setMaxNotationToRecipe(RecipeNote rn, Recipe recipe) {
        recipeNoteRepository.save(rn);
        Notification notification = new Notification();
        notification.setIdUsers(recipe.getIdOwner());
        notification.setNotificationId(1L);
        notification.setDescription("Féléciation! Vous avez reçu une notation de 5 étoiles !");
        return notificationRepository.save(notification);
    }

    public String deleteRecipe(Long idRecipe, Long idUser) {
        Recipe recipe = recipeRepository.findById(idRecipe)
                .orElseThrow(NotFoundException::new);
        if (Objects.equals(recipe.getIdOwner().getUserId(), idUser)) {
            recipeRepository.deleteById(idRecipe);
            return "La recette a bien été supprimée.";
        } else {
            return "Vous n'avez pas les droit pour modifier cette recette.";
        }
    }

    public String updateRecipe(Long idRecipe, Long userId, Recipe updatedRecipe) {
        Recipe recipe = recipeRepository.findById(idRecipe)
                .orElseThrow(NotFoundException::new);
        if (Objects.equals(recipe.getIdOwner().getUserId(), userId)) {
            if (updatedRecipe.getName() != null) {
                recipe.setName(updatedRecipe.getName());
            }
            if (updatedRecipe.getSharingMode() != null) {
                recipe.setSharingMode(updatedRecipe.getSharingMode());
            }
            recipeRepository.save(recipe);
            return "La recette a bien été modifiée.";
        } else {
            return "Vous n'avez pas les droit pour modifier cette recette.";
        }
    }

    public String addFriend(Long idUser, Long idFriend) {
        Users users = userRepository.findById(idUser)
                .orElseThrow(NotFoundException::new);
        Users friend = userRepository.findById(idFriend)
                .orElseThrow(NotFoundException::new);

        List<Friendship> list1 = friendshipRepository.findByIdUsersAndIdFriend(friend, users);
        List<Friendship> list2 = friendshipRepository.findByIdUsersAndIdFriend(users, friend);

        if (list1.isEmpty() && list2.isEmpty()) {
            if (friendshipRepository.countByIdUsers(users) < 10 && friendshipRepository.countByIdUsers(friend) < 10) {
                Friendship fs = new Friendship();
                fs.setIdFriend(friend);
                fs.setIdUsers(users);
                fs.setFriendshipId(friendshipRepository.count());
                friendshipRepository.save(fs);
                return "Une nouvelle amité s'est créée avec " + fs.getIdFriend().getFirstname() + "!";
            }
        } else {
            return "Vous amitié existe déjà.";
        }
        return "Impossible de créer une nouvelle amitié... ";
    }

    public String deleteFriend(Long idUser, Long idFriendShip) {
        Users users = userRepository.findById(idUser).orElseThrow(NotFoundException::new);
        Optional<Friendship> f = friendshipRepository.findById(idFriendShip);
        if (friendshipRepository.countByIdUsers(users) == 0){
            return "Encore faut-il avoir des amis pour en supprimer.";
        }
        if (f.isPresent()){
            if (Objects.equals(f.get().getIdUsers().getUserId(), users.getUserId())) {
                friendshipRepository.deleteById(f.get().getFriendshipId());
                Optional<Users> friend = userRepository.findById(f.get().getIdFriend().getUserId());
                return friend.map(value -> "C'est la fin de l'amitié entre " + value.getFirstname() + " et vous...").orElse("L'amitié a bien été brisée.");
            } else {
                return "Vous n'avez pas les droits pour mettre fin à cette amitié.";
            }
        } else {
            return "Cette amitié n'a jamais existée.";
        }
    }

    public List<Friendship> getAllFriendsOfUser(Long idUser) {
        Users users = userRepository.findById(idUser)
                .orElseThrow(NotFoundException::new);
        return friendshipRepository.findAllByIdUsers(users);
    }

    public void deleteAllUser() {
        userRepository.deleteAll();
    }

    public Integer createRecipe(final Recipe r, final Long idUser) {
        final Recipe recipe = new Recipe();
        final Optional<Users> user = userRepository.findById(idUser);
        if (user.isPresent()) {
            recipe.setName(r.getName());
            recipe.setRecipeId(recipeRepository.count());
            recipe.setCreationDate(new Date());
            recipe.setIdOwner(user.get());
            recipe.setLastOwnerVisitDate(null);
            recipe.setSharingMode(r.getSharingMode());
        }
        return Math.toIntExact(recipeRepository.save(recipe).getRecipeId());
    }
}
