package org.khizo.api.service;

import org.khizo.api.model.RecipeNote;
import org.khizo.api.repository.RecipeNoteRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeNoteService {
    @Autowired
    private RecipeNoteRepository recipeNoteRepository;

    public List<RecipeNote> getAllRecipeNote() {
        return recipeNoteRepository.findAll();
    }

    public Optional<RecipeNote> getRecipeNoteById(final Long id) {
        return recipeNoteRepository.findById(id);
    }

    public void updateRecipeNote(RecipeNote rn, Long id) {
        final RecipeNote recipeNote = recipeNoteRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        recipeNoteRepository.save(mapToEntity(rn, recipeNote));
    }

    private RecipeNote mapToEntity(final RecipeNote rn,
                                     final RecipeNote recipeNote) {
        recipeNote.setNote(rn.getNote());
        recipeNote.setIdRecipe(rn.getIdRecipe());
        recipeNote.setIdUsers(rn.getIdUsers());
        recipeNote.setFavorite(rn.getFavorite());
        recipeNote.setRecipeNoteId(rn.getRecipeNoteId());
        return recipeNote;
    }

    public Integer createRecipeNote(final RecipeNote rn) {
        final RecipeNote recipeNote = new RecipeNote();
        mapToEntity(rn, recipeNote);
        return Math.toIntExact(recipeNoteRepository.save(recipeNote).getRecipeNoteId());
    }

    public void deleteRecipeNote(Integer id) {
        recipeNoteRepository.deleteById(Long.valueOf(id));
    }

    public void deleteAllRecipeNote() {
        recipeNoteRepository.deleteAll();
    }
}
