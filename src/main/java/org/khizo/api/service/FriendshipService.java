package org.khizo.api.service;

import org.khizo.api.model.Friendship;
import org.khizo.api.repository.FriendshipRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FriendshipService {

    @Autowired
    private FriendshipRepository friendshipRepository;

    public List<Friendship> getAllFriendship() {
        return friendshipRepository.findAll();
    }

    public Optional<Friendship> getFriendshipById(Long id) {
        return friendshipRepository.findById(id);
    }

    public void updateFriendship(Friendship fs, int id) {
        final Friendship friendship = friendshipRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        friendshipRepository.save(mapToEntity(fs, friendship));
    }

    private Friendship mapToEntity(final Friendship fs, final Friendship friendship) {
        friendship.setFriendshipId(fs.getFriendshipId());
        friendship.setIdUsers(fs.getIdUsers());
        friendship.setIdFriend(fs.getIdFriend());
        return friendship;
    }
    public Integer createFriendship(final Friendship f) {
        final Friendship friendship = new Friendship();
        mapToEntity(f, friendship);
        return Math.toIntExact(friendshipRepository.save(friendship).getFriendshipId());
    }

    public void deleteFriendship(Integer id) {
        friendshipRepository.deleteById(Long.valueOf(id));
    }

    public void deleteAllFriendship() {
        friendshipRepository.deleteAll();
    }
}
