package org.khizo.api.service;

import org.khizo.api.model.Configuration;
import org.khizo.api.repository.ConfigurationRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConfigurationService {

    @Autowired
    private ConfigurationRepository configurationRepository;

    public List<Configuration> getAllConfiguration() {
        return configurationRepository.findAll();
    }

    public Optional<Configuration> getConfigurationById(int id) {
        return configurationRepository.findById(id);
    }

    public void updateConfiguration(Configuration conf, int id) {
        final Configuration configuration = configurationRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        configurationRepository.save(mapToEntity(conf, configuration));
    }

    public void deleteConfiguration(Integer id) {
        configurationRepository.deleteById(id);
    }

    public Long createConfiguration(final Configuration conf) {
        final Configuration configuration = new Configuration();
        mapToEntity(conf, configuration);
        return configurationRepository.save(configuration).getConfId();
    }

    private Configuration mapToEntity(final Configuration conf,
                                      final Configuration configuration) {
        configuration.setConfName(conf.getConfName());
        configuration.setConfValue(conf.getConfValue());
        configuration.setConfId(conf.getConfId());
        return configuration;
    }
}
