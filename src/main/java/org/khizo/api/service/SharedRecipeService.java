package org.khizo.api.service;

import org.khizo.api.model.SharedRecipe;
import org.khizo.api.repository.SharedRecipeRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SharedRecipeService {

    @Autowired
    private SharedRecipeRepository sharedRecipeRepository;

    public List<SharedRecipe> getAllSharedRecipe() {
        return sharedRecipeRepository.findAll();
    }

    public Optional<SharedRecipe> getSharedRecipeById(int id) {
        return sharedRecipeRepository.findById((long) id);
    }

    public void updateSharedRecipe(SharedRecipe sr, int id) {
        final SharedRecipe sharedRecipe = sharedRecipeRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        sharedRecipeRepository.save(mapToEntity(sr, sharedRecipe));
    }

    private SharedRecipe mapToEntity(final SharedRecipe sr,
                                   final SharedRecipe sharedRecipe) {
        sharedRecipe.setIdRecipe(sr.getIdRecipe());
        sharedRecipe.setIdFriend(sr.getIdFriend());
        sharedRecipe.setSharedRecipeId(sr.getSharedRecipeId());
        return sharedRecipe;
    }
    public Integer createSharedRecipe(final SharedRecipe sr) {
        final SharedRecipe sharedRecipe = new SharedRecipe();
        mapToEntity(sr, sharedRecipe);
        return Math.toIntExact(sharedRecipeRepository.save(sharedRecipe).getSharedRecipeId());
    }

    public void deleteSharedRecipe(Integer id) {
        sharedRecipeRepository.deleteById(Long.valueOf(id));
    }

}
