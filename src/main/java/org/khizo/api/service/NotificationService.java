package org.khizo.api.service;

import org.khizo.api.model.Notification;
import org.khizo.api.repository.NotificationRepository;
import org.khizo.api.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    public List<Notification> getAllNotification() {
        return notificationRepository.findAll();
    }

    public Optional<Notification> getNotificationById(int id) {
        return notificationRepository.findById((long) id);
    }

    public void updateNotification(Notification n, int id) {
        final Notification notification = notificationRepository.findById((long) id)
                .orElseThrow(NotFoundException::new);
        notificationRepository.save(mapToEntity(n, notification));
    }

    private Notification mapToEntity(final Notification n,
                                   final Notification notification) {
        notification.setNotificationId(n.getNotificationId());
        notification.setDescription(n.getDescription());
        notification.setIdUsers(n.getIdUsers());
        return notification;
    }

    public Integer createNotification(final Notification n) {
        final Notification notification = new Notification();
        mapToEntity(n, notification);
        return Math.toIntExact(notificationRepository.save(notification).getNotificationId());
    }

    public void deleteNotification(Integer id) {
        notificationRepository.deleteById(Long.valueOf(id));
    }
}
